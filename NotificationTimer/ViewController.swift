//
//  ViewController.swift
//  NotificationTimer
//
//  Created by James Cash on 10-04-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet weak var numberOfSecondsField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func startTimer(_ sender: UIButton) {
        guard let secondsText = numberOfSecondsField.text else { return }
        guard let seconds = Double(secondsText) else { return }

        print("timer for \(seconds)s")
        let content = UNMutableNotificationContent()
        content.title = "Timer done"
        content.body = "Timer for \(seconds) seconds ended"

        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: seconds, repeats: false)
        let request = UNNotificationRequest(identifier: "timerRequest", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error) in
            if let err = error {
                print("An error occured: \(err)")
            }
            print("Added timer")
        }

    }

}

